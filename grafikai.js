  new Chartist.Line('#chart1', {
    labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
    series: [[300, 150, 250, 100, 300, 150, 250, 500, 100, 300, 150, 250]]
  });


var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Sportiniai bateliai", "Žemakulniai bateliai", "Aukštakulniai bateliai", "Basutės", "Auliniai batai", "Ilgaauliai batai"],
        datasets: [{
            label: 'Pokytis',
            data: [4, 11, 2, 3, 5, 7],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

var ctx = document.getElementById("myChartLine").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["Sausis", "Vasaris", "Kovas", "Balandis", "Gegužė", "Birželis", "Liepa", "Rugpjūtis"],
        datasets: [{
            label: 'Pokytis',
            data: [100, 200, 350, 50, 100, 150, 250, 350],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
                'rgba(255,99,132,1)',
            ],
            borderWidth: 3
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

new Chart(document.getElementById("pie-chart"), {
    type: 'pie',
    data: {
      labels: ["Išskirtinumas", "Suderinamumas", "Spalva", "Kaina", "Kokybė", "Patogumas/komfortas"],
      datasets: [{
        label: "Svarbiausia renkantis batus:",
        backgroundColor: ["#e1bee7", "#ce93d8","#ba68c8","#9c27b0","#7b1fa2", "#4a148c"],
        data: [35, 15, 5, 10, 30, 5]
      }]
    },
    options: {
      title: {
        display: true,
        text: ''
      }
    }
});

