$(document).ready(function() {
	$("#web-menu").dropdown({
		coverTrigger: false,
		hover: true
	});	
	$("#mobile-menu").dropdown();	
});

$(document).ready(function(){
    $('.sidenav').sidenav();
  });

$('.carousel').carousel();

function myMap() {
    var mapOptions = {
    	center: new google.maps.LatLng(54.6870191, 25.2813931),
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.TERRAIN
    }
var map = new google.maps.Map(document.getElementById("map"), mapOptions);
}
