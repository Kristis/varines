var ctx = document.getElementById("stulpelinis").getContext('2d');
var mychart = new Chart (ctx, {
    type: 'bar',
    data: {
      labels: ["Sportiniai", "Tapkės"],
      datasets: [
        {
          label: "Dažnumas (%)",
          backgroundColor: ["#6600cc", "#ff9966"],
          data: [66,34]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Dažnumas (%)'
      }
    }
});

var ctx = document.getElementById("linijinis");
var mychart = new Chart (ctx, {
  type: 'line',
  data: {
    labels: [1,2,3,4],
    datasets: [{ 
        data: [100,0,0,130],
        label: "Mama duoda",
        borderColor: "#3e95cd",
        fill: false
      }, { 
        data: [20,0,19,40],
        label: "Bernas duoda",
        borderColor: "#8e5ea2",
        fill: false
      }, { 
        data: [30,10,15,45],
        label: "Aukoku savo atlyginimą",
        borderColor: "#3cba9f",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Pinigų suma (per metų ketvirtį, eurais)'
    }
  }
});

var ctx = document.getElementById("polarinis");
var mychart = new Chart (ctx, {
    type: 'polarArea',
    data: {
      labels: ["Bukanosių lakierkų", "Juodų aukštakulnių", "Balerinkų be dekoracijų"],
      datasets: [
        {
          label: "Kaip smarkiai nemėgstu? (nemėgimo skalė iki 10)",
          backgroundColor: ["#ff9966", "#6600cc","#ffcc99"],
          data: [4,9,2]
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Kaip smarkiai nemėgstu? (nemėgimo skalė iki 10'
      }
    }
});