new Chart(document.getElementById("doughnut-chart"), {
    type: 'doughnut',
    data: {
      labels: ["Aukstakulniai", "Zemakulniai", "Sportiniai", "Ilgaauliai", "Basutes"],
      datasets: [
        {
          label: "Batai mano spintoje",
          backgroundColor: ["#f06292", "#aa00ff","#26a69a","#80d8ff","#c45850"],
          data: [100,200,150,99,80]
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: ''
      }
    }
});

var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Aukstakulniai", "Zemakulniai", "Sportiniai", "Ilgaauliai", "Basutes"],
        datasets: [{
            label: '#',
            data: [3050,2500,980,1600,300],
            backgroundColor: [
                'rgba(255, 102, 204, 0.2)',
                'rgba(0, 102, 255, 0.2)',
                'rgba(204, 255, 51, 0.2)',
                'rgba(153, 0, 204, 0.2)',
                'rgba(255, 102, 0, 0.2)',
            ],
            borderColor: [
                'rgba(255, 102, 204,1)',
                'rgba(0, 102, 255, 1)',
                'rgba(204, 255, 51, 1)',
                'rgba(153, 0, 204, 1)',
                'rgba(255, 102, 0, 1)',
            ],
            borderWidth: 2
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

var ctx = document.getElementById("myChartLine").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["Aukstakulniai", "Zemakulniai", "Sportiniai", "Ilgaauliai", "Basutes"],
        datasets: [{
            label: '#',
            data: [5,2,10,24,36],
            backgroundColor: [
                'rgba(102, 0, 204, 0.2)',
            ],
            borderColor: [
                'rgba(102, 0, 204,1)',
            ],
            borderWidth: 3
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

